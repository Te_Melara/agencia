<%@page import="org.proyectosfgk.persistencia.Destinos"%>
<%@page import="org.proyectosfgk.persistencia.TipoClase"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="org.proyectosfgk.utils.JPAUtils"%>
<%@page import="org.proyectosfgk.utils.JPAUtils"%>
<jsp:include page="../layouts/encabezadoPrin.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Estilo/terrestre.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<center>
        <h1>Compra de boleto</h1>
        <hr>
        <form border="2" action="detaBus.jsp" method="post">                    
            <div>
                <input type="radio" name="ida" value="Ida" checked/>Ida
                <input type="radio" name="ida" value="IdaVuelta"/>Ida y Vuelta
            </div>
            
            <br>
            <div class="ida">
                <label  class="titulo" id="terr"> Fecha de Ida </label><br>
                <input placeholder="Fecha de Ida" name="fec_ida" type="text" id="campofecha">
            </div>
            
            <br>
            <div class="regreso">
                <label class="titulo" id="terr"> Fecha de Regreso </label><br>
                <input placeholder="Fecha de Regreso" name="fec_regre" type="text" id="campofecha2">             
            </div>          
            <br>
            <div>
                <%
                  
                EntityManager em = JPAUtils.getEntityManagerFactory().createEntityManager();
                List<Destinos> destinos = em.createNamedQuery("Destinos.findTerrestre", Destinos.class).getResultList();
                List<Object[]> destinosES = em.createNativeQuery("SELECT d.id, d.nombre, ci.nombre FROM destinos d INNER JOIN ciudad ci ON d.id_ciudad = ci.id WHERE d.nombre LIKE 'ter%' AND ci.id_pais = 1").getResultList();
                List<TipoClase> clase = em.createNamedQuery("TipoClase.findAll", TipoClase.class).getResultList();
                %>
                <label class="titulo" id="terr"><i style="font-size:24px" class="fa">&#xf041;</i> Terminal de Origen </label><br>
                <select name="origen">
                    <% for(Object[] d : destinosES){%>
                    <option value="<%=d[0]%>"><%=d[2]%> - <%=d[1]%></option>
                    <%}%>
                </select>             
            </div>
            
            <br>
            <div>
                <label class="titulo" id="terr"><i style="font-size:24px" class="fa">&#xf041;</i> Terminal de Destino </label><br>
                <select name="destino">
                    <% for(Destinos d : destinos){%>
                    <option value="<%=d.getId()%>"><%=d.getIdCiudad().getNombre()%> - <%=d.getNombre()%></option>
                    <%}%>
                </select>
            </div> 
           
            <br>
            <label class="titulo" colspan="2" id="terr"> N�mero de Pasajeros </label>
            <br>
            <div>
                    <label class="titulo" id="terr"> Adulto </label>
                    <select style="width: 60px" name="num_adul">
                        <option>0</option>
                        <option selected="">1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
              
                    <label class="titulo" id="terr"> Joven </label>
                    <select style="width: 60px" name="num_jov">
                        <option selected="">0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                    
                    <label> Clase </label>
                    <select name="clase">
                        <% for (TipoClase c : clase) {%>
                        <option value="<%=c.getId()%>"><%=c.getClase()%></option>
                        <%}%>
                    </select>             
            </div>
            <br>
            <div>
                <input type="submit" name="siguiente" value="Siguiente" style="text-align: right" />             
            </div>

        </form>
    </center>
                <script src="../js/jquery-3.3.1.min.js" type="text/javascript"></script>
                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        
        $("input[name='ida']").on('click', function(){
            var viajes = $("input[name='ida']:checked").val();
        
            if(viajes == "ida"){
                $('.regreso').css('display', 'none');          
            }else{
                $('.regreso').css('display', 'block');
                
            }
        });
        
        $(function() {
          $("#campofecha" ).datepicker({
            numberOfMonths: 1,
                    showButtonPanel: true
                });
                $("#campofecha2" ).datepicker({
            numberOfMonths: 1,
                    showButtonPanel: true
                });
            });
       
      
    </script>

<jsp:include page="../layouts/pieEmp.jsp"/>
