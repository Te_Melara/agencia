<jsp:include page="../layouts/encabezadoPrin.jsp"/>
<%
    String tipo = (request.getParameter("viaje") != null)? request.getParameter("viaje"): "";
    int origen = (request.getParameter("origen") != null)? Integer.parseInt(request.getParameter("origen")): 0;
    int destino = (request.getParameter("destino") != null)? Integer.parseInt(request.getParameter("destino")): 0;
    String partida = (request.getParameter("partida") != null) ? request.getParameter("partida"): "";
    String regreso = (request.getParameter("regreso") != null)? request.getParameter("regreso"): "";
    int origen1 = (request.getParameter("origen1") != null)? Integer.parseInt(request.getParameter("origen1")): 0;
    int destino2 = (request.getParameter("destino2") != null) ? Integer.parseInt(request.getParameter("destino2")): 0;
    String partida1 = (request.getParameter("partida1") != null)? request.getParameter("partida1"): "";
    int origen2 = (request.getParameter("origen2") != null)? Integer.parseInt(request.getParameter("origen2")): 0;
    int destino3 = (request.getParameter("destino3") != null) ? Integer.parseInt(request.getParameter("destino3")): 0;
    String partida2 = (request.getParameter("partida2") != null) ? request.getParameter("partida2"): "";
    int cantAdult = (request.getParameter("cantAdul") != null) ? Integer.parseInt(request.getParameter("cantAdul")): 0;
    int cantJoven = (request.getParameter("cantJove") != null) ? Integer.parseInt(request.getParameter("cantJove")): 0;
    int clase = (request.getParameter("claseVia") !=null) ? Integer.parseInt(request.getParameter("claseVia")): 0;
    
%>
<center>
        <h1>Detalle del viaje!</h1>
        
        <form action="../compra.do" method="post" id="detalle">           
            <input type="hidden" name="accion" value="agregar">
            <table id="formul" border="1" cellpadding="4" style="border-collapse: collapse;" >
                <% if (tipo.equals("ida")) {%>
                <tr>
                    <td id="estatic">Tipo de viaje:</td>
                    <td id="datos"><%=tipo%></td>

                </tr>
                <tr>
                    <td id="estatic">Origen:</td>
                    <td id="datos"><%=nom_origen%></td>
                </tr>
                <tr>
                    <td id="estatic">Destino:</td>
                    <td id="datos"><%=nom_desti%></td>
                </tr>
                <tr>
                    <td id="estatic">Partida:</td>
                    <td id="datos"><%=partida%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de adultos:</td>
                    <td id="datos"><%=cantAdult%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de Jovenes:</td>
                    <td id="datos"><%=cantJoven%></td>
                </tr>
                <tr>
                    <td id="estatic">Clase:</td>
                    <td id="datos"><%=nom_clase%></td>
                </tr>
                <%} else if (tipo.equals("idaVuelta")) {%>
                <tr>
                    <td id="estatic">Tipo de viaje: </td>
                    <td id="datos"><%=tipo%></td>

                </tr>
                <tr>
                    <td id="estatic">Origen:</td>
                    <td id="datos"><%=nom_origen%></td>
                </tr>
                <tr>
                    <td id="estatic">Destino:</td>
                    <td id="datos"><%=nom_desti%></td>
                </tr>
                <tr>
                    <td id="estatic">Partida:</td>
                    <td id="datos"><%=partida%></td>
                </tr>
                <tr>
                    <td id="estatic">Regreso:</td>
                    <td id="datos"><%=regreso%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de adultos:</td>
                    <td id="datos"><%=cantAdult%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de Jovenes:</td>
                    <td id="datos"><%=cantJoven%></td>
                </tr>
                <tr>
                    <td id="estatic">Clase:</td>
                    <td id="datos"><%=nom_clase%></td>
                </tr>
                <%} else {%>
                <tr>
                    <td id="estatic">Tipo de viaje: </td>
                    <td id="datos"><%=tipo%></td>                 
                </tr>
                <tr>
                    <td id="estatic">Origen:</td>
                    <td id="datos"><%=nom_origen%></td>
                </tr>
                <tr>
                    <td id="estatic">Destino:</td>
                    <td id="datos"><%=nom_desti%></td>
                </tr>
                <tr>
                    <td id="estatic">Partida:</td>
                    <td id="datos"><%=partida%></td>
                </tr>
                <tr>
                    <td id="estatic">Origen:</td>
                    <td id="datos"><%=nom_origen1%></td>

                </tr>
                <tr>
                    <td id="estatic">Destino:</td>
                    <td id="datos"><%=nom_desti2%></td>
                </tr>
                <tr>
                    <td id="estatic">Partida:</td>
                    <td id="datos"><%=partida1%></td>
                </tr>
                <tr>
                    <td id="estatic">Origen:</td>
                    <td id="datos"><%=nom_origen2%></td>
                </tr>
                <tr>
                    <td id="estatic">Destino:</td>
                    <td id="datos"><%=nom_desti3%></td>
                </tr>
                <tr>
                    <td id="estatic">Partida:</td>
                    <td id="datos"><%=partida2%></td>
                </tr>              
                <tr>
                    <td id="estatic">Numero de adultos:</td>
                    <td id="datos"><%=cantAdult%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de Jovenes:</td>
                    <td id="datos"><%=cantJoven%></td>
                </tr>
                <tr>
                    <td id="estatic">Clase:</td>
                    <td id="datos"><%=nom_clase%></td>
                </tr>
                <%}%>
                <tr>
                    <td id="estatic">Precio:</td>
                    <td id="datos"></td>
                </tr>
                <br>
                <tr>
                    <td>
                <input type="submit" name="agregar" value="Comprar"/>
                    </td>
                    <td>
                <input type="submit" name="reservar" value="Reservar"/>
                    </td>
                </tr>
            </table>           
        </form>
</center>
<jsp:include page="../layouts/pieEmp.jsp"/>
