<%@page import="org.proyectosfgk.persistencia.Destinos"%>
<%@page import="org.proyectosfgk.persistencia.TipoClase"%>
<%@page import="java.sql.ResultSet"%>
<jsp:include page="../layouts/encabezadoPrin.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Estilo/terrestre.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Estilo/table.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js"></script> 

<%
    String tipoV = (request.getParameter("ida") != null)? request.getParameter("ida"): "";
    String fec_ida = (request.getParameter("fec_ida") !=null) ? request.getParameter("fec_ida"): "";
    String fec_regre = (request.getParameter("fec_regre") !=null) ? request.getParameter("fec_regre"): "";
    
    int origen = (request.getParameter("origen") !=null) ? Integer.parseInt(request.getParameter("origen")): 0;
    int destino = (request.getParameter("destino") !=null) ? Integer.parseInt(request.getParameter("destino")): 0;
    
    int cantAdult = (request.getParameter("num_adul") != null) ? Integer.parseInt(request.getParameter("num_adul")): 0;
    int cantJoven = (request.getParameter("num_jov") != null) ? Integer.parseInt(request.getParameter("num_jov")): 0;
    int clase = (request.getParameter("clase") !=null) ? Integer.parseInt(request.getParameter("clase")): 0;
    double total = 0;
    
    Destinos des = new Destinos();
    String nom_origen = des.getNombre();
    
    TipoClase ter = new TipoClase();
    
   %>

<h1>Detalle de viaje Terrestre!</h1>
        
        <form id="formul2" method="post">
            <table id="infor">
                <% if (tipoV.equals("Ida")){%>
                <tr>
                    <td id="estatic">Tipo de viaje:</td>
                    <td id="datos"><%=tipoV%></td>
                </tr>
                <tr>
                    <td id="estatic">Fecha de ida:</td>
                    <td id="datos"><%=fec_ida%></td>
                </tr>
                <tr>
                    <td id="estatic">Origen:</td>
                    <td id="datos"><%=origen%></td>
                </tr>
                <tr>
                    <td id="estatic">Destino:</td>
                    <td id="datos"><%=destino%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de Adultos:</td>
                    <td id="datos"><%=cantAdult%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de jovenes:</td>
                    <td id="datos"><%=cantJoven%></td>
                </tr>
                <tr>
                    <td id="estatic">Clase:</td>
                    <td id="datos"><%=clase%></td>
                </tr>
                <%}else{%>
                <tr>
                    <td id="estatic">Tipo de viaje:</td>
                    <td id="datos"><%=tipoV%></td>
                </tr>
                <tr>
                    <td id="estatic">Fecha de Ida:</td>
                    <td id="datos"><%=fec_ida%></td>
                </tr>
                 <tr>
                    <td id="estatic">Fecha de Regreso:</td>
                    <td id="datos"><%=fec_regre%></td>
                </tr>
                <tr>
                    <td id="estatic">Origen:</td>
                    <td id="datos"><%=origen%></td>
                </tr>
                <tr>
                    <td id="estatic">Destino:</td>
                    <td id="datos"><%=destino%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de Adultos:</td>
                    <td id="datos"><%=cantAdult%></td>
                </tr>
                <tr>
                    <td id="estatic">Numero de jovenes:</td>
                    <td id="datos"><%=cantJoven%></td>
                </tr>
                <tr>
                    <td id="estatic">Clase:</td>
                    <td id="datos"><%=clase%></td>
                </tr>
                <%}%>
                <tr>
                    <td id="estatic">Precio a pagar:</td>
                    <td id="datos">$<%= total%></td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" name="accion" value="Comprar"/>
                    </td>
                    <td>
                        <input type="submit" name="reservar" value="Reservar"/>
                    </td>
                
                </tr>
            </table>
        </form>

<jsp:include page="../layouts/pieEmp.jsp"/>
