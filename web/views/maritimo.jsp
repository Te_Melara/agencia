
<jsp:include page="../layouts/encabezadoPrin.jsp"/>

        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Estilo/Style4.css" />
        
    <div id="contenido">
        <div class="container">
            <div class="div-img hidden" ><a title="Bahamas" href="bahamas.jsp"><img  class="img" src="https://www.viajarencruceros.com/sites/viajarencruceros.com/files/Ofertas-de-crucero-para-estas-vacaciones.jpg" 
                                                                          alt="viaje1"  width="350" height="250" />
                    <div class="overlay"></div></a>
                <div class="text" id="texto1">BAHAMAS</div>
            </div>
            </div> 
        <div class="container4">
            <div class="div-img hidden" >                        
                <a title="Mar Caribe" href="caribe.jsp?id=2"><img  class="img" src="https://as01.epimg.net/tikitakas/imagenes/2018/01/28/portada/1517162893_793270_1517240090_noticia_normal.jpg" 
                                                    alt="viaje2" width="350" height="250" />
                <div class="overlay"></div></a>
                <div class="text" id="texto2">MAR  CARIBE</div>
            </div>
        </div>  
        <div class="container2">
            <div class="div-img hidden" ><a title="Mar Mediterraneo" href="mediterraneo.jsp"><img class="img" src="https://i0.wp.com/www.turistum.com/wp-content/uploads/2015/11/cruceros-por-el-caribe.jpg" 
                                                                                  alt="viaje3"  width="450" height="350"/>
                <div class="overlay"></div></a>
                <div class="text" >MAR MEDITERRANEO</div>
            </div>
        </div>  
        <div class="container3">
            <div class="div-img hidden" >
                <a title="Islas Canarias" href="i_canarias.jsp"><img class="img" src="https://respuestas.tips/wp-content/uploads/2017/11/vacaciones-canarias-en-septiembre-1024x684.jpg"
                                                       alt="viaje4" width="350" height="250" />
                <div class="overlay"></div></a>
                <div class="text" id="texto4">ISLAS CANARIAS</div>
            </div>
        </div> 
    </div>
    <jsp:include page="../layouts/pieEmp.jsp"/>