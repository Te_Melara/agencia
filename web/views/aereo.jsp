<%@page import="org.proyectosfgk.persistencia.TipoClase"%>
<%@page import="javax.persistence.TypedQuery"%>
<%@page import="java.util.List"%>
<%@page import="org.proyectosfgk.persistencia.Destinos"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="org.proyectosfgk.utils.JPAUtils"%>
<jsp:include page="../layouts/encabezadoPrin.jsp"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<center>
        <br><br>
        <form id="estiform" action="detaAvion.jsp" method="post">    
            <br>
            <div>
                <h3>Reserva hoy...Compra hoy!!</h3>
            </div>
            <hr>               
            <div>              
                <input type="radio"  name="viaje" value="ida" checked> Solo Ida 
                <input type="radio"  name="viaje" value="idaVuelta" > Ida y Vuelta
                <input type="radio" name="viaje" value="escala" > Escala                
            </div>
            <br>
            <div>
                <label><i style="font-size:24px" class="fa">&#xf041;</i> Origen </label><br>
                <%
                EntityManager em = JPAUtils.getEntityManagerFactory().createEntityManager();
                List<Destinos> destinos = em.createNamedQuery("Destinos.findAereo", Destinos.class).getResultList();
                List<Object[]> destinosES = em.createNativeQuery("SELECT d.id, d.nombre, ci.nombre FROM destinos d INNER JOIN ciudad ci ON d.id_ciudad = ci.id WHERE d.id = 38 AND ci.id_pais = 1").getResultList();
                List<TipoClase> clase = em.createNamedQuery("TipoClase.findAll", TipoClase.class).getResultList();
                %>
                <select name="origen">
                    <% for(Object[] d : destinosES){%>
                    <option value="<%=d[0]%>"><%=d[2]%> - <%=d[1]%></option>
                    <%}%>
                </select>
            </div>
            <br>
           <div>
                <label><i style="font-size:24px" class="fa">&#xf041;</i> Destino </label><br>
                
                <select name="destino">
                    <% for(Destinos d : destinos){%>
                    <option><%=d.getIdCiudad().getNombre()%> - <%=d.getNombre()%></option>
                    <%}%>
                </select>
            </div>
            <br>         
            <div>
                <label><i style="font-size:24px" class="fa">&#xf073;</i> Partida </label>
                <input type="date" name="partida"/>
            </div>        
            <br>
            <div class="regre">
                <label><i style="font-size:24px" class="fa">&#xf073;</i> Regreso </label>
                <input type="date" name="regreso"/>
            </div>
            <br>
            <div class="ocultar">
                <label><i style="font-size:24px" class="fa">&#xf041;</i> Origen </label><br>
                <select name="origen2">
                    <% for(Destinos d : destinos){%>
                    <option><%=d.getIdCiudad().getNombre()%> - <%=d.getNombre()%></option>
                    <%}%>
                </select>               
            </div>  
            <br>
            <div class="ocultar">
                <label><i style="font-size:24px" class="fa">&#xf041;</i> Destino </label><br>
                <select name="destino2">
                    <% for(Destinos d : destinos){%>
                    <option><%=d.getIdCiudad().getNombre()%> - <%=d.getNombre()%></option>
                    <%}%>
                </select>              
            </div>  
            <br>
            <div class="ocultar">
                <label><i style="font-size:24px" class="fa">&#xf073;</i> Partida </label>
                <input type="date" name="partida1"/>                  
            </div>  
            <br>
            <div class="ocultar">
                <label><i style="font-size:24px" class="fa">&#xf041;</i> Origen </label><br>
                <select name="origen3">
                    <% for(Destinos d : destinos){%>
                    <option><%=d.getIdCiudad().getNombre()%> - <%=d.getNombre()%></option>
                    <%}%>
                </select>
            </div>
            <br>
            <div class="ocultar">
                <label><i style="font-size:24px" class="fa">&#xf041;</i> Destino </label><br>
                <select name="destino3">
                    <% for(Destinos d : destinos){%>
                    <option><%=d.getIdCiudad().getNombre()%> - <%=d.getNombre()%></option>
                    <%}%>
                </select>               
            </div> 
            <br>
            <div class="ocultar">
                <label><i style="font-size:24px" class="fa">&#xf073;</i> Partida </label>
                <input type="date" name="partida2"/>
            </div>
            <br>
            <div>
                <label> Adulto </label>
                <select style="width: 60px" name="cantAdul">
                    <option>0</option>
                    <option selected="">1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                </select>

                <label> Joven </label>
                <select style="width: 60px" name="cantJove">
                    <option>0</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                </select>

                <label> Clase </label>               
                <select name="clase">
                    <% for (TipoClase c : clase) {%>
                    <option><%=c.getClase()%></option>
                    <%}%>
                </select>
                
            </div>
            <br>
            <div>
                <input type="submit" name="Siguiente" value="Siguiente" style="text-align: left" />               
            </div>
        </form>
    </center>

    <script src="../js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script>
        
        $("input[name='viaje']").on('click', function(){
            var viajes = $("input[name='viaje']:checked").val();
        
            if(viajes == "escala"){
                $('.ocultar').css('display', 'block');
            }else if(viajes == "ida"){
                $('.regre').css('display', 'none');
            }
            else{
                $('.ocultar').css('display', 'none');
                $('.regre').css('display', 'block');
            }
        });
      
    </script>
<jsp:include page="../layouts/pieEmp.jsp"/>
