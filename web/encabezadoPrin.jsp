<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Estilo/Style.css" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/images/Logofinal.png" />
        <title>KCT Travel Agency</title>
    </head>
    <body>
        <header class="prin">
            <img src="${pageContext.request.contextPath}/images/Logofinal.png" alt="" width="93px" height="100px"/> 
            <ul id="ini">
                <li class="inicio"><a href="${pageContext.request.contextPath}/views/login.jsp">Iniciar Sesión</a></li>
                <li class="regis"><a href="${pageContext.request.contextPath}/views/registro.jsp">Resgistrarse</a></li>
            </ul>
        </header>
    <center>
        <ul id="nav">
            <li class="inic"><a href="${pageContext.request.contextPath}/index.jsp">Inicio</a></li>
            <li class="aereo"><a href="${pageContext.request.contextPath}/views/aereo.jsp">Aereo</a></li>
            <li class="terres"><a href="${pageContext.request.contextPath}/views/terrestre.jsp">Terrestre</a></li>
            <li class="mari"><a href="${pageContext.request.contextPath}/views/maritimo.jsp">Maritimo</a></li>     
        </ul>
    </center>
    <br><br><br><br><br>
    <h2 style="text-align: center">BIENVENIDOS A KCT  TRAVEL AGENCY</h2><br><br>