/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.persistencia;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tania.melarafgkss
 */
@Entity
@Table(name = "escala")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Escala.findAll", query = "SELECT e FROM Escala e")
    , @NamedQuery(name = "Escala.findById", query = "SELECT e FROM Escala e WHERE e.id = :id")
    , @NamedQuery(name = "Escala.findByOrigen", query = "SELECT e FROM Escala e WHERE e.origen = :origen")
    , @NamedQuery(name = "Escala.findByDestino", query = "SELECT e FROM Escala e WHERE e.destino = :destino")
    , @NamedQuery(name = "Escala.findByFecPartida", query = "SELECT e FROM Escala e WHERE e.fecPartida = :fecPartida")
    , @NamedQuery(name = "Escala.findByIdViaje", query = "SELECT e FROM Escala e WHERE e.idViaje = :idViaje")})
public class Escala implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "origen")
    private String origen;
    @Column(name = "destino")
    private String destino;
    @Column(name = "fec_partida")
    @Temporal(TemporalType.DATE)
    private Date fecPartida;
    @Column(name = "id_viaje")
    private Integer idViaje;

    public Escala() {
    }

    public Escala(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getFecPartida() {
        return fecPartida;
    }

    public void setFecPartida(Date fecPartida) {
        this.fecPartida = fecPartida;
    }

    public Integer getIdViaje() {
        return idViaje;
    }

    public void setIdViaje(Integer idViaje) {
        this.idViaje = idViaje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Escala)) {
            return false;
        }
        Escala other = (Escala) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.persistencia.Escala[ id=" + id + " ]";
    }
    
}
