/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.persistencia;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tania.melarafgkss
 */
@Entity
@Table(name = "crucero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Crucero.findAll", query = "SELECT c FROM Crucero c")
    , @NamedQuery(name = "Crucero.findById", query = "SELECT c FROM Crucero c WHERE c.id = :id")
    , @NamedQuery(name = "Crucero.findByNombre", query = "SELECT c FROM Crucero c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Crucero.findByDestino", query = "SELECT c FROM Crucero c WHERE c.destino = :destino")
    , @NamedQuery(name = "Crucero.findByDuracion", query = "SELECT c FROM Crucero c WHERE c.duracion = :duracion")
    , @NamedQuery(name = "Crucero.findByFecha", query = "SELECT c FROM Crucero c WHERE c.fecha = :fecha")})
public class Crucero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "destino")
    private String destino;
    @Column(name = "duracion")
    private Integer duracion;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    public Crucero() {
    }

    public Crucero(Integer id) {
        this.id = id;
    }

    public Crucero(Integer id, Date fecha) {
        this.id = id;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Crucero)) {
            return false;
        }
        Crucero other = (Crucero) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.persistencia.Crucero[ id=" + id + " ]";
    }
    
}
