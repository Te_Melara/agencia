/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tania.melarafgkss
 */
@Entity
@Table(name = "destinos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Destinos.findAll", query = "SELECT d FROM Destinos d")
    , @NamedQuery(name = "Destinos.findById", query = "SELECT d FROM Destinos d WHERE d.id = :id")
    , @NamedQuery(name = "Destinos.findByNombre", query = "SELECT d FROM Destinos d WHERE d.nombre = :nombre")
    , @NamedQuery(name = "Destinos.findAereo", query = "SELECT d FROM Destinos d WHERE d.nombre LIKE 'aer%'")
    , @NamedQuery(name = "Destinos.findTerrestre", query = "SELECT d FROM Destinos d WHERE d.nombre LIKE 'ter%'")})
public class Destinos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @PrimaryKeyJoinColumn(name = "id_viaje", referencedColumnName = "id")
    @ManyToOne
    private TipoViaje idViaje;
    @JoinColumn(name = "id_viaje", referencedColumnName = "id")
    @ManyToOne
    private TipoTrans idViaje1;
    @JoinColumn(name = "id_ciudad", referencedColumnName = "id")
    @ManyToOne
    private Ciudad idCiudad;
    @OneToMany(mappedBy = "origen")
    private List<Viaje> viajeList;
    @OneToMany(mappedBy = "destino")
    private List<Viaje> viajeList1;

    public Destinos() {
    }

    public Destinos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoViaje getIdViaje() {
        return idViaje;
    }

    public void setIdViaje(TipoViaje idViaje) {
        this.idViaje = idViaje;
    }

    public TipoTrans getIdViaje1() {
        return idViaje1;
    }

    public void setIdViaje1(TipoTrans idViaje1) {
        this.idViaje1 = idViaje1;
    }

    public Ciudad getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Ciudad idCiudad) {
        this.idCiudad = idCiudad;
    }

    @XmlTransient
    public List<Viaje> getViajeList() {
        return viajeList;
    }

    public void setViajeList(List<Viaje> viajeList) {
        this.viajeList = viajeList;
    }

    @XmlTransient
    public List<Viaje> getViajeList1() {
        return viajeList1;
    }

    public void setViajeList1(List<Viaje> viajeList1) {
        this.viajeList1 = viajeList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Destinos)) {
            return false;
        }
        Destinos other = (Destinos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.persistencia.Destinos[ id=" + id + " ]";
    }
    
}
