/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.persistencia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tania.melarafgkss
 */
@Entity
@Table(name = "tipo_clase")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoClase.findAll", query = "SELECT t FROM TipoClase t")
    , @NamedQuery(name = "TipoClase.findById", query = "SELECT t FROM TipoClase t WHERE t.id = :id")
    , @NamedQuery(name = "TipoClase.findByClase", query = "SELECT t FROM TipoClase t WHERE t.clase = :clase")
    , @NamedQuery(name = "TipoClase.findByPreAdulto", query = "SELECT t FROM TipoClase t WHERE t.preAdulto = :preAdulto")
    , @NamedQuery(name = "TipoClase.findByPreJoven", query = "SELECT t FROM TipoClase t WHERE t.preJoven = :preJoven")
    , @NamedQuery(name = "TipoClase.findByClaAereo", query = "SELECT t FROM TipoClase t WHERE t.preAdulto  > 100 AND t.preJoven > 100")
})
public class TipoClase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "clase")
    private String clase;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "pre_adulto")
    private BigDecimal preAdulto;
    @Column(name = "pre_joven")
    private BigDecimal preJoven;
    @OneToMany(mappedBy = "idClase")
    private List<Viaje> viajeList;

    public TipoClase() {
    }

    public TipoClase(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public BigDecimal getPreAdulto() {
        return preAdulto;
    }

    public void setPreAdulto(BigDecimal preAdulto) {
        this.preAdulto = preAdulto;
    }

    public BigDecimal getPreJoven() {
        return preJoven;
    }

    public void setPreJoven(BigDecimal preJoven) {
        this.preJoven = preJoven;
    }

    @XmlTransient
    public List<Viaje> getViajeList() {
        return viajeList;
    }

    public void setViajeList(List<Viaje> viajeList) {
        this.viajeList = viajeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoClase)) {
            return false;
        }
        TipoClase other = (TipoClase) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.persistencia.TipoClase[ id=" + id + " ]";
    }
    
}
