/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.persistencia;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tania.melarafgkss
 */
@Entity
@Table(name = "viaje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Viaje.findAll", query = "SELECT v FROM Viaje v")
    , @NamedQuery(name = "Viaje.findById", query = "SELECT v FROM Viaje v WHERE v.id = :id")
    , @NamedQuery(name = "Viaje.findByFecPartida", query = "SELECT v FROM Viaje v WHERE v.fecPartida = :fecPartida")
    , @NamedQuery(name = "Viaje.findByFecRegreso", query = "SELECT v FROM Viaje v WHERE v.fecRegreso = :fecRegreso")
    , @NamedQuery(name = "Viaje.findByNAdulto", query = "SELECT v FROM Viaje v WHERE v.nAdulto = :nAdulto")
    , @NamedQuery(name = "Viaje.findByNJoven", query = "SELECT v FROM Viaje v WHERE v.nJoven = :nJoven")})
public class Viaje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fec_partida")
    @Temporal(TemporalType.DATE)
    private Date fecPartida;
    @Column(name = "fec_regreso")
    @Temporal(TemporalType.DATE)
    private Date fecRegreso;
    @Column(name = "n_adulto")
    private Integer nAdulto;
    @Column(name = "n_joven")
    private Integer nJoven;
    @OneToMany(mappedBy = "idViaje")
    private List<Reservacion> reservacionList;
    @JoinColumn(name = "id_clase", referencedColumnName = "id")
    @ManyToOne
    private TipoClase idClase;
    @JoinColumn(name = "id_tiViaje", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoViaje idtiViaje;
    @JoinColumn(name = "origen", referencedColumnName = "id")
    @ManyToOne
    private Destinos origen;
    @JoinColumn(name = "destino", referencedColumnName = "id")
    @ManyToOne
    private Destinos destino;

    public Viaje() {
    }

    public Viaje(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecPartida() {
        return fecPartida;
    }

    public void setFecPartida(Date fecPartida) {
        this.fecPartida = fecPartida;
    }

    public Date getFecRegreso() {
        return fecRegreso;
    }

    public void setFecRegreso(Date fecRegreso) {
        this.fecRegreso = fecRegreso;
    }

    public Integer getNAdulto() {
        return nAdulto;
    }

    public void setNAdulto(Integer nAdulto) {
        this.nAdulto = nAdulto;
    }

    public Integer getNJoven() {
        return nJoven;
    }

    public void setNJoven(Integer nJoven) {
        this.nJoven = nJoven;
    }

    @XmlTransient
    public List<Reservacion> getReservacionList() {
        return reservacionList;
    }

    public void setReservacionList(List<Reservacion> reservacionList) {
        this.reservacionList = reservacionList;
    }

    public TipoClase getIdClase() {
        return idClase;
    }

    public void setIdClase(TipoClase idClase) {
        this.idClase = idClase;
    }

    public TipoViaje getIdtiViaje() {
        return idtiViaje;
    }

    public void setIdtiViaje(TipoViaje idtiViaje) {
        this.idtiViaje = idtiViaje;
    }

    public Destinos getOrigen() {
        return origen;
    }

    public void setOrigen(Destinos origen) {
        this.origen = origen;
    }

    public Destinos getDestino() {
        return destino;
    }

    public void setDestino(Destinos destino) {
        this.destino = destino;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Viaje)) {
            return false;
        }
        Viaje other = (Viaje) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.persistencia.Viaje[ id=" + id + " ]";
    }
    
}
