
package org.proyectosfgk.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.proyectosfgk.models.ViajeAereo;
import org.proyectosfgk.persistencia.Destinos;
import org.proyectosfgk.persistencia.TipoClase;
import org.proyectosfgk.persistencia.Viaje;
import org.proyectosfgk.utils.JPAUtils;

public class comprasController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        EntityManager em = JPAUtils.getEntityManagerFactory().createEntityManager();
        
        String tipoV = (request.getParameter("ida") != null) ? request.getParameter("ida") : "";
        String fec_ida = (request.getParameter("fec_ida") != null) ? request.getParameter("fec_ida") : "";
        String fec_regre = (request.getParameter("fec_regre") != null) ? request.getParameter("fec_regre") : "";

        int origen = (request.getParameter("origen") != null) ? Integer.parseInt(request.getParameter("origen")) : 0;
        int destino = (request.getParameter("destino") != null) ? Integer.parseInt(request.getParameter("destino")) : 0;

        int cantAdult = (request.getParameter("num_adul") != null) ? Integer.parseInt(request.getParameter("num_adul")) : 0;
        int cantJoven = (request.getParameter("num_jov") != null) ? Integer.parseInt(request.getParameter("num_jov")) : 0;
        int clase = (request.getParameter("clase") != null) ? Integer.parseInt(request.getParameter("clase")) : 0;

        
        String accion = request.getParameter("accion");
         Viaje vi = new Viaje();
        if(accion.equals("Comprar")){   
          em.getEntityManagerFactory().createEntityManager();
          em.getTransaction().begin();
          
          Destinos d = new Destinos();
          d.setId(origen);        
          vi.setOrigen(d);
          
          d.setId(destino);
          vi.setDestino(d);
          
          SimpleDateFormat f = new SimpleDateFormat("YYYY-MM-DD");
            try {
                Date dat = f.parse(fec_ida);
                vi.setFecPartida(dat);
            } catch (ParseException ex) {
                Logger.getLogger(comprasController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
                Date dat = f.parse(fec_regre);
                vi.setFecRegreso(dat);
            } catch (ParseException ex) {
                Logger.getLogger(comprasController.class.getName()).log(Level.SEVERE, null, ex);
            }          
            vi.setNAdulto(cantAdult);
            vi.setNJoven(cantJoven);
            
            TipoClase cla = new TipoClase();
            cla.setId(clase);
            vi.setIdClase(cla);
            
            em.persist(vi);
            em.getTransaction().commit();
            em.close(); 
            if(ViajeAereo.agregarViaje(vi))
                System.out.println("Compra con exito");
        }else{
            System.out.println("Compra fallo");
        }
        request.getRequestDispatcher("aereo.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
