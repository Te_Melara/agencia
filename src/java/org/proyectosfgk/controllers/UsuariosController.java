
package org.proyectosfgk.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.proyectosfgk.persistencia.Roles;
import org.proyectosfgk.persistencia.Usuarios;
import org.proyectosfgk.utils.JPAUtils;
import org.proyectosfgk.utils.Validacion;


@WebServlet(name = "UsuariosController", urlPatterns = {"/UsuariosController"})
public class UsuariosController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //processRequest(request, response);
        EntityManager em = JPAUtils.getEntityManagerFactory().createEntityManager();
        
        String accion = request.getParameter("accion");
 
        String username = "";
        int idRol = 0;
        if(request.getParameter("user")!= null)
            username = request.getParameter("user");
        
        
        Usuarios u = new Usuarios();
        
        switch(accion){
            case "entrar":
                em.getEntityManagerFactory().createEntityManager();
                em.getTransaction().begin();
                try{
                u= em.createNamedQuery("Usuarios.findByUsuario", Usuarios.class).setParameter("usuario",username).getSingleResult();
                
                
                System.out.println("Clave" + u.getClave());
                
                em.getTransaction().commit();
                em.close();
                

                if(u.getClave().equals(request.getParameter("clave"))&& u.getUsuario().equals(request.getParameter("user"))){
                                       
                   
                        request.getRequestDispatcher("/views/aereo.jsp").forward(request, response); 
                }else{
                    
                     String error ="Usuario y/o contraseña incorrectos";
                    
                    request.getRequestDispatcher("/views/login.jsp?error=" + error).forward(request, response); 
                 }
                
                }catch (Exception ex){
                    
                    String error ="Usuario y/o contraseña incorrectos";
                    
                    request.getRequestDispatcher("/views/login.jsp?error=" + error).forward(request, response);
                }
                

                break;
                
                case"Agregar":
                String mensaje = "";
                //Agregar
                try {
                    String clave = request.getParameter("pass").trim();
                    clave = Validacion.obtenerContrasenaEncriptada(clave);
                    em.getTransaction().begin();
                    Usuarios r = new Usuarios();
                    r.setNombres(request.getParameter("nombre"));
                    r.setApellidos(request.getParameter("apellido"));
                    r.setDireccion(request.getParameter("direc"));
                    r.setCorreo(request.getParameter("correo"));
                    r.setUsuario(request.getParameter("usu"));
                    r.setClave(clave);
                    em.persist(r);
                    em.getTransaction().commit();
                    em.close();
                    mensaje = "Se ingreso con exito";
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                } catch (Exception ex) {
                    mensaje = "Ya existe este usuario";
                }
                break;

        } 
    
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
