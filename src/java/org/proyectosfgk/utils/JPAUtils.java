
package org.proyectosfgk.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAUtils {
    private static final EntityManagerFactory EMF =Persistence.createEntityManagerFactory("AgenciaPU");
    
    public static EntityManagerFactory getEntityManagerFactory(){
        
        return EMF;
    }
    
    
}
