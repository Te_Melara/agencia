
package org.proyectosfgk.models;

import javax.persistence.EntityManager;
import org.proyectosfgk.persistencia.Viaje;
import org.proyectosfgk.utils.JPAUtils;

public class ViajeAereo {
    
    public static boolean agregarViaje(Viaje v){
        
        boolean agregar = false;
        try{
            EntityManager em = JPAUtils.getEntityManagerFactory().createEntityManager();
            em.getTransaction().begin();
            em.persist(v);
            
            em.getTransaction().commit();
            em.close();
            
            agregar= true;
        }catch(Exception ex){
            System.out.println("Error" + ex.getMessage());
        }
        return agregar;
    }
}
